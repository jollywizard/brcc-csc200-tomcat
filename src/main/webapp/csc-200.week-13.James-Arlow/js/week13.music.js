$(function() {
	initMusic();
	nextTrack();
});

function initMusic() {
	window.lastTrack = null;
	window.player = $('#funky_sauce')[0];
	window.nowPlayin = $('#now-playin');

	window.tracklist = [
		{ name:'redshift', url:'music/8bp124-01-exilefaker-redshift.mp3', site:'http://www.8bitpeoples.com/products/520435-exilefaker-ockhams-chainsaw'}
	,	{ name:'orbitalfrontaldistal', url:'music/8bp124-02-exilefaker-orbitalfrontaldistal.mp3', site:'http://www.8bitpeoples.com/products/520435-exilefaker-ockhams-chainsaw'}
	,	{ name:'ydodo', url:'music/8bp075-03-kplecraft-ydodo.mp3', site:'http://www.8bitpeoples.com/products/520257-kplecraft-multi-boxer'}
	,	{ name:'shudder', url:'music/04 Shudder.mp3', site:'http://www.8bitpeoples.com/products/520417-cheapshot-zomg'}
	,   { name:'peedeetwo', url:'music/8BP108-01-jellica-peedeetwo.mp3', site:'http://www.8bitpeoples.com/products/520407-jellica-desmos'}
	];
	
	player.addEventListener('ended', nextTrack);
}

function nextTrack() {
	if (tracklist.length > 0) {
		var selected = randomIntFromInterval(0,tracklist.length - 1);
		setTrack(selected);
		//
		tracklist.splice(selected,1);
	}
}

function setTrack(index) {
	track = tracklist[index];

	player = window.player;
	player.src = track.url;
	player.load();
	player.play();
	
	nowPlayin.text(track.name);
	lastTrack = index;
	nowPlayin.attr('href', track.site);
}

//Via stack overflow.
function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}
