

$.fn.randoCalrissian = function() {
	//Via stack overflow.
	var randomizer = function (min,max)	{
		return Math.floor(Math.random()*(max-min+1)+min);
	};
	
	var randomFontColor = function () {
		var r = randomizer(125,255);
		var g = randomizer(125,255);
		var b = randomizer(125,255);
		
		var css = "#"+r.toString(16) + g.toString(16) + b.toString(16);
		return css;
	};
	
	var font_list = [
		"Palatino",
		"Times",
		"Arial",
		"cursive",
		"Charcoal",
		"Helvetica",
		"Courier",
		"Monaco",
		"Lucida Grande"
	];
	
	var select = randomizer(0, font_list.length - 1);
	var font = font_list[select];
	this.css('font-family', font);
	
	var color = randomFontColor();
	this.css('color', randomFontColor());
	
	return this;
}