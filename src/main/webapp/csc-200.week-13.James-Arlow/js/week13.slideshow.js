$(function() {
	setStage();
	randomSlide();
	setInterval(randomSlide, 3750);
});

function setStage() {
	var height = $(window).height();
	var width = $(window).width();
	
	var stage = $('#slideshow');
	
	var top = stage.offset().top;
	var musicBox = $('#music-box');
	var bottom = musicBox.offset().top;
	
	stage.height(bottom - top - 20);
	stage.width(width * .75);
	
	stage.position({
		my: "center top"
	,	at: "center bottom"
	,	of: $('#h1')
	});
}

function randomSlide() {
	randomHeader();
	slide(randomDescription());
}

function randomHeader() {
	$('#h1').randoCalrissian();
}

function randomDescription() {
	var index = randomIndex(0, descriptions().size() - 1);
	return descriptions().get(index);
}

//Via stack overflow.
function randomIndex(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

function descriptions() {
	return $('#descriptions li');
}

function slide(element) {
	var stage = $('#slideshow');
	stage.empty();
	
	element = $(element);
	var message = $('.message' , element).clone();
		message.text(message.text().toUpperCase());
		
		//week-13 edit: font randomizer.
		message.randoCalrissian();
	
	var image = $('.image' , element).clone()
		.height(stage.height() * .8)
		.css('max-width', stage.width())
		.css('max-height', stage.height());

	stage.append(image);
	stage.append(message);
		
	image.position({
		my: "center center"
	,	at: "center center"
	,	of: stage
	});
	message.position({
		my: "center bottom-5"
	,	at: "center top"
	,	of: image
	});

}